﻿using System;

namespace HomeWork1.App
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] options = new[] { "Let the sun beat down upon my face", "The forests will echo with laughter", "Been dazed and confused for so long",
                                       "I am gonna ramble on, sing my song", "Upon us all a little rain must fall" };

            var esc = ConsoleKey.Escape;
            ConsoleKeyInfo cki;

            Console.WriteLine("Press 'Enter' to start writing a line.");
            cki = Console.ReadKey(true);

            do {
                Random rnd = new Random();

                DateTime startedAt = DateTime.Now;

                Console.WriteLine("\n" + options[rnd.Next(0, options.Length)]);
                Console.ReadLine();

                TimeSpan span = DateTime.Now - startedAt;

                Console.WriteLine($"\nYour print speed is {span.TotalSeconds}\n");

                Console.WriteLine("Let's try one more time!");
                Console.WriteLine("Press 'Enter' to start writing a line or 'Esc' to quit.");
                cki = Console.ReadKey(true);
            } while (cki.Key != esc);
        }
    }
}
